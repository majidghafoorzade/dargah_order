import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        order: {
            plan: '',
            domain: '',
            addons: {
                mainContent: false,
                seo: false,
                relocate: false
            }
        },
        price: {
            plan : 0,
            domain: 0,
            addons: {
                mainContent: 0,
                seo: 0,
                relocate: 0
            }
        },
        appBased: 'plan',
    },

    mutations: {
        planPrice(state, payload){
            this.state.price.plan = payload.amount;
        },
    },

    getters: {
        overallPrice(state){
            return (state.price.plan + state.price.domain + state.price.addons.mainContent + state.price.addons.seo + state.price.addons.relocate);
        }
    }
})
