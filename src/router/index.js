import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Order from '../views/Order.vue'
import Plan from '../views/Plan.vue'
import Domain from '../views/Domain.vue'
import Settings from '../views/Settings.vue'
import Services from '../views/Services.vue'
import Final from '../views/Final.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: Order,
    children: [
      {
        path: '',
        name: 'plan',
        component: Plan
      },
      {
        path: 'domain',
        name: 'domain',
        component: Domain
      },
      {
        path: 'settings',
        name: 'settings',
        component: Settings
      },
      {
        path: 'services',
        name: 'services',
        component: Services
      },
      {
        path: 'final',
        name: 'final',
        component: Final
      },
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
