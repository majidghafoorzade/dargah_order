import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import axios from 'axios'

Vue.config.productionTip = false;
Vue.prototype.$http = axios;

new Vue({
  router,
  store,
  created(){
    let htmlEl=document.querySelector("html");
     htmlEl.setAttribute('dir','rtl');
     htmlEl.setAttribute('lang','fa');
  },
  render: h => h(App)
}).$mount('#app')
